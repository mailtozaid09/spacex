import 'react-native-gesture-handler';
import * as React from 'react';
import { LogBox, StatusBar } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/screens/HomeScreen';
import DetailsScreen from './src/screens/DetailsScreen';
import Colors from './src/styles/Colors';

LogBox.ignoreAllLogs(true);

const Stack = createStackNavigator();

const App = () => {
    return (
        <NavigationContainer>
            <StatusBar animated={true} backgroundColor={Colors.DARK_BLUE} />
            <Stack.Navigator>
                <Stack.Screen
                    name="Home"
                    component={HomeScreen}
                    options={{
                        headerShown: false
                    }}
                />
                <Stack.Screen 
                    name="Details" 
                    component={DetailsScreen}
                    options={{
                        headerShown: false
                    }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default App;