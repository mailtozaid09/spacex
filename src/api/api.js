
const baseUrl = 'https://api.spacexdata.com/v4/';

// Get Launch Pads 
export function getLaunchPads() {
    return(
        fetch(
            baseUrl + 'launchpads',
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}


// Get Launch 
export function getLaunches() {
    return(
        fetch(
            baseUrl + 'launches/',
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}