import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, TouchableOpacity, Image, FlatList} from 'react-native';
import Colors from '../styles/Colors';
import Divider from './divider/Divider';

import { useNavigation } from '@react-navigation/native';
const {width} = Dimensions.get('window');


const LaunchPad = (props) => {

    const navigation = useNavigation();

    const {data, launchesData} = props;

    const capitalizeLetter = (str) => {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    const LaunchItem = ({data, launchId, }) => {

        return(
            <View>
                {data.map((item) => (
                    item.id === launchId
                    ?
                    <TouchableOpacity onPress={() => navigation.navigate('Details', {details: item})} activeOpacity={0.4} style={styles.launchIcon} >
                        <Image
                            source={{uri: item.links.patch.large}}
                            style={{height: width*0.16, width: width*0.16}}
                        />
                    </TouchableOpacity>
                    :
                    null
                ))}
                
            </View>
        )
    }

    const renderItem = ({ item }) => {
        return(
            <View style={styles.cardContainer} >
                <View style={styles.cardContent} >
                    <Image
                        source={{uri: item.images.large[0]}}
                        style={{height: 200, borderTopLeftRadius: 20, borderTopRightRadius: 20}}
                    />

                    <View style={styles.statusContainer} >
                        <View style={styles.statusContent} >
                            {item.status === 'retired' ? <Image source={require('../../assets/retired.png')} style={styles.statusIcon} /> : null }
                            {item.status === 'active' ? <Image source={require('../../assets/active.png')} style={styles.statusIcon} /> : null }
                            {item.status === 'under construction' ? <Image source={require('../../assets/construction.png')} style={styles.statusIcon} /> : null }
                            <Text style={styles.statusText} >{capitalizeLetter(item.status)}</Text>
                        </View>
                    </View>

                    <View style={styles.textContainer} >
                        <View>
                            <Text style={styles.title} >{item.name}</Text>
                        </View>
                        <View>
                            <Text style={styles.details} numberOfLines={3} >{item.details}</Text>
                        </View>

                        <View style={{marginTop: 10,}} >
                            <Divider />
                        </View>

                        <View style={styles.launchesContainer} >
                            {item.launches.length !== 0
                            ?
                            <View>
                                <Text style={styles.noLaunches} >Top Launches</Text>
                                <View style={styles.launchIconContainer} >
                                    {item.launches.slice(0, 3).map((item) => (
                                        <LaunchItem data={launchesData} launchId={item} /> 
                                    ))} 
                                </View>
                            </View>
                            :
                            <View>
                                <Text style={styles.noLaunches} >No Launch Available</Text>
                             </View>
                           
                            }
                        
                        </View>
                    </View>
                </View>
            </View>
        )
    }
    
    return (
        <View style={{flex: 1, marginTop: 10}} >
           <FlatList
                data={data}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    cardContainer: {
        padding: 15,
        paddingTop: 5,
        borderRadius: 20,
    },
    cardContent: {
        height: 300,
        borderWidth: 2,
        borderRadius: 20,
        borderColor: Colors.WHITE,
    },
    launchesContainer: {
        paddingTop: 10
    },
    statusContainer: {
        position: 'absolute',
        top: 10,
        right: 10
    },
    statusContent: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 4,
        justifyContent: 'space-between', 
        borderRadius: 10,
        backgroundColor: Colors.WHITE
    },
    statusText: {
        fontSize: 14,
        lineHeight: 16,
        marginLeft: 4,
        color: Colors.BLUE
    },
    textContainer: {
        width: '100%',
        position: 'absolute',
        bottom: 0,
        padding: 10,
        borderRadius: 20,
        backgroundColor: Colors.BLUE
    },
    title: {
        fontSize: 18,
        lineHeight: 26,
        color: Colors.WHITE,
    },
    details: {
        fontSize: 12,
        lineHeight: 16,
        color: Colors.WHITE
    },
    noLaunches: {
        fontSize: 12,
        lineHeight: 12,
        color: Colors.WHITE
    },
    launchIconContainer: {
        marginTop: 10,
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between',
    },
    launchIcon: {
        borderWidth: 3,
        padding: 5,
        borderRadius: 20,
        borderColor: Colors.LIGHT_PURPLE,
    },
    statusIcon: {
        height: 22, width: 22
    },
    center: {
        alignItems: 'center',
        width: '100%',
    },
})

export default LaunchPad