import React, {  } from 'react'
import { Image, View, StyleSheet, TouchableOpacity } from 'react-native'
import Colors from '../../styles/Colors'
import { useNavigation } from '@react-navigation/native';

const BackButton = () => {

    const navigation = useNavigation();

    return (
        <TouchableOpacity onPress={() => navigation.goBack()} style={styles.container} >
            <Image source={require('../../../assets/backArrow.png')} style={styles.icon} /> 
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 44,
        width: 44,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: Colors.BLUE,
        backgroundColor: Colors.WHITE, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    icon: {
        height: 22,
        width: 22,
        resizeMode: 'contain'
    }
})
export default BackButton
