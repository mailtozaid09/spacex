import React, {  } from 'react'
import { Text, View, StyleSheet, Dimensions } from 'react-native'
import Colors from '../../styles/Colors';

const {width} = Dimensions.get('window');

const Divider = (props) => {
    return (
        <View style={[styles.container, props.style]} >
            <View style={[styles.divider, props.dividerStyle]} />
        </View>
    )
}

export default Divider


const styles = StyleSheet.create({
    container: {
        marginTop: 0,
    },
    divider: {
        height: 1,
        backgroundColor: Colors.GRAY
    }
})