import React, {  } from 'react'
import { Text, View, StyleSheet, } from 'react-native'
import Colors from '../../styles/Colors'

const Header = (props) => {

    const {title} = props;
    return (
        <View style={styles.container} >
            <Text style={styles.text} >{title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingLeft: 15,
        paddingTop: 10,
    },
    text: {
        fontSize: 20,
        lineHeight: 20,
        color: Colors.WHITE
    }
})
export default Header
