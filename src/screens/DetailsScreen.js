import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, TouchableOpacity, ImageBackground, Image, FlatList} from 'react-native';
import Colors from '../styles/Colors';
import Moment from 'moment';
import BackButton from '../components/button/BackButton';

const {width, height} = Dimensions.get('window');

const DetailsScreen = (props) => {

    const [itemDetails, setItemDetails] = useState(props.route.params.details);

    return (
        <View>
            <ImageBackground
                source={require('../../assets/bg.png')}
                style={{width: '100%', height: '100%',}}
            >
                <View>
                   
                    <View style={{height: 330, backgroundColor: Colors.WHITE}} >
                        <Image
                            source={{uri: itemDetails.links.patch.large}}
                            style={{height: 330, resizeMode: 'contain'}}
                        />
                         <View style={styles.buttonStyle} >
                            <BackButton />
                        </View>
                    </View>
                   
                   <View>
                        <View style={styles.cardContainer} >
                            <View style={styles.cardContent} >
                                <View>
                                    <Text style={styles.title} >{itemDetails.name}</Text>
                                </View>
                                <View>
                                    <Text style={styles.dateStyle} >{Moment(itemDetails.date_local).format('MMM D, YYYY - HH:mm A')}</Text>
                                </View>

                                <View style={styles.reusedContainer} >
                                    <Text style={styles.reusedText} >{itemDetails.fairings && itemDetails.fairings.reused ? 'Reused' : 'Not Reused'}</Text>
                                </View>
                                <View>
                                    <Text numberOfLines={10} style={styles.details} >{itemDetails.details ? itemDetails.details : 'No Description!'}</Text>
                                </View>
                            </View>
                        </View>
                   </View>
                </View>
            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    cardContent: {
        borderWidth: 2,
        margin: 20,
        padding: 20,
        marginTop: 0,
        borderRadius: 20,
        backgroundColor: Colors.BLUE,
        borderColor: Colors.WHITE
    },
    reusedContainer: {
        width: 90,
        height: 30,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 12,
        backgroundColor: Colors.WHITE
    },
    reusedText: {
        fontSize: 14,
        lineHeight: 16,
        fontWeight: 'bold',
        color: Colors.BLACK
    },
    cardContainer: {
        width: '100%',
        position: 'absolute',
        top: -30,
    },
    title: {
        fontSize: 24,
        lineHeight: 26,
        color: Colors.WHITE,
    },
    details: {
        fontSize: 16,
        lineHeight: 22,
        color: Colors.WHITE
    },
    dateStyle: {
        fontSize: 14,
        lineHeight: 16,
        marginBottom: 10,
        marginTop: 5,
        color: Colors.LIGHT_GRAY
    },
    buttonStyle: {
        position: 'absolute',
        top: 15,
        left: 15
    }
})

export default DetailsScreen