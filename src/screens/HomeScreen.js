import React, {useState, useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, ImageBackground, TouchableOpacity, Image, FlatList} from 'react-native';

import { getLaunchPads, getLaunches } from '../api/api';
import Header from '../components/header/Header';
import LaunchPad from '../components/LaunchPad';
import Loader from '../components/loader/Loader';

const HomeScreen = () => {
    
    const [data, setData] = useState([]);
    const [launchesData, setLaunchesData] = useState([]);
    const [loader, setLoader] = useState(true);

    useEffect(() => {
        getLaunchPadsFunc()
        
    }, [])


    const getLaunchPadsFunc = () => {

        console.log("getLaunchPadsFunc-----");
        getLaunchPads()
            .then((response) => {
                console.log("RESPONSE---");
                //console.log(response);
                setData(response);
                getLaunchesFunc();
            })
            .catch((error) => {
                console.log("ERROR---");
                console.log(error);
            })
    }

    const getLaunchesFunc = () => {

        console.log("getLaunchesFunc-----");
        getLaunches()
            .then((response) => {
                console.log("RESPONSE---");
                //console.log(response);
                setLaunchesData(response);
                setLoader(false)
            })
            .catch((error) => {
                console.log("ERROR---");
                console.log(error);
            })
    }
    

    return (
        <View style={styles.container} >
            {loader
            ?
            <Loader />
            :
            <View>
                <ImageBackground
                  source={require('../../assets/bg.png')}
                  style={{width: '100%', height: '100%',}}
                >
                    <Header title="Launch Pads" />
                    <LaunchPad data={data} launchesData={launchesData} />
                </ImageBackground>
            </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})

export default HomeScreen