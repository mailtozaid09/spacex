const Colors = {
    RED: 'red',
    BLACK: '#000',
    WHITE: '#fff',
    GRAY: '#C0C0C0',
    BLUE: '#1D1690',
    DARK_BLUE: '#0d3bc7',
    LIGHT_GRAY: '#d4d4d4',
    LIGHT_BLACK: '#212121',
    LIGHT_PURPLE: '#6a5acd'
}

export default Colors